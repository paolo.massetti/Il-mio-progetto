<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShowController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\SearchController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/announcement/create', [AnnouncementController::class, 'create'])->name('announcement.create');
Route::post('/announcement/store', [AnnouncementController::class, 'store'])->name('announcement.store');
Route::get('/category/{name}/{id}/announcements', [HomeController::class, 'announcementsByCategory'])->name('announcements.category');
Route::get('/announcement/{announcement}/show', [HomeController::class, 'show'])->name('announcement.show');
Route::get('/search', [SearchController::class, 'search'])->name('search');
//chiamata ajax
Route::post('/announcement/images/upload', [AnnouncementController::class, 'uploadImage'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove', [AnnouncementController::class, 'removeImage'])->name('announcement.images.remove');
Route::get('/announcement/images', [AnnouncementController::class, 'getImages'])->name('announcement.images');

//linguaggi
Route::post('/locale/{locale}', [HomeController::class, 'locale'])->name('locale');



//lavora come revisore
Route::get('/mail/create', [ContactController::class, 'create'])->name('mail.create');
Route::post('/contact/revisor', [ContactController::class, 'contact'])->name('contact.send');

//area revisore
Route::middleware(['auth.revisor'])->group(function () {
    Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.home');
    Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
    Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');
    
//rejected
Route::get('/revisor/deleted', [RevisorController::class, 'getDeletedAnnouncements'])->name('revisor.deleted');
Route::delete('/announcement/destroy/{announcement}', [RevisorController::class, 'destroy'])->name('announcement.destroy');

});



    
    
