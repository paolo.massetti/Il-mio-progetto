<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionRemoveFaces;
use App\Jobs\WatermarkerImages;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $uniqueSecret = $request->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));
        $categories = Category::all();
        return view('announcement.create', compact('categories', 'uniqueSecret'));
        

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {
        
        $announcement = Announcement::create([

            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'category_id' => $request->input('category_id'),
            'user_id' => Auth::id(),
            'price' => $request->input('price'),
            
            $uniqueSecret = $request->input('uniqueSecret'),

            ]);

            $images = session()->get("images.{$uniqueSecret}", []);
            $removedImages = session()->get("removedimages.{$uniqueSecret}", []);
            $images = array_diff($images, $removedImages);

            foreach ($images as $image) {
                
                $img = AnnouncementImage::create([
                    $fileName = basename($image),
                    $newFileName = "public/announcements/{$announcement->id}/{$fileName}",
                    Storage::move($image, $newFileName),
                    // dispatch(new ResizeImage($newFileName, 300, 230)),
                    'file' => $newFileName,
                    'announcement_id' => $announcement->id
                ]); 

                // dispatch(new GoogleVisionSafeSearchImage($img->id));
                // dispatch(new GoogleVisionLabelImage($img->id));
                // dispatch(new GoogleVisionRemoveFaces($img->id));

                    // ->

                GoogleVisionSafeSearchImage::withChain([
                    new GoogleVisionLabelImage($img->id),
                    new GoogleVisionRemoveFaces($img->id),
                    new ResizeImage($img->file, 300, 230),
                    new ResizeImage($img->file,480, 380 ),
                    
                    
                ])->dispatch($img->id);
            }


            File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));


            return redirect( route('home'))->with('status', "l'annuncio $announcement->title è stato inserito correttamente");
            
            

      
    }
            



    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage($fileName, 120, 120));
        
        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(['id' => $fileName]);

    

    }

    public function removeImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
    
        return response()->json('ok');
       
    }

    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);
        $images = array_diff($images, $removedImages);

        $data = [];

        // foreach ($images as $image) {
        //     $data[] = [
        //         'id' => $image,
        //         'src' => Storage::url($image)
        //     ];
        // }


        foreach ($images as $image) {
            $data[] = [
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }


        return response()->json($data);


    }

   

    


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }

    
}
