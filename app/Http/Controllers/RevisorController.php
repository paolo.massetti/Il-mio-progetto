<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Http\Middleware\RevisorMiddleware;




class RevisorController extends Controller
{
    public function __contruct()
    {
        // $this->middleware('auth.revisor');
    }

    public function index()
    {
        $announcement = Announcement::where('is_accepted', null)->orderBy('id', 'desc')->first();
        
        return view('revisor.home', compact('announcement'));

    }

    private function setAccepted($announcement_id, $value)
    {
        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value;
        $announcement->save();
        return redirect(route('revisor.home'));
    }


    public function accept($announcement_id)
    {
        return $this->setAccepted($announcement_id, true);
    }

    public function reject($announcement_id)
    {
        return $this->setAccepted($announcement_id, false);
    }

    

    public function getDeletedAnnouncements()
    {
        $announcements = Announcement::where('is_accepted', false)->get();
        
        return view('revisor.deleted', compact('announcements'));
    }

    public function destroy(Announcement $announcement)
    {
        $announcement->delete();
        return redirect()->back();
    }

    
}

