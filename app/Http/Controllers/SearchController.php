<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{

    public function search(Request $request)
    {
        $q = $request->input('q');

        $announcements = Announcement::search($q)->get();
        return view('announcement.search', compact('q', 'announcements'));
    }
    

}

