<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $announcements = Announcement::where('is_accepted', true)->orderBy('id','desc')->take(6)->get();
        return view('home', compact('announcements'));
    }

    public function announcementsByCategory($name, $category_id)
    {
        $category = Category::find($category_id);
        $announcements = $category->announcements()->where('is_accepted', true)->orderBy('id','desc')->paginate(5);
        return view('announcements', compact('category', 'announcements'));
    }
    
    public function locale($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function show(Announcement $announcement)
    {
        $category = $announcement->category;
        $announcements = Announcement::all();


        $announcementsForCategory = $announcements->filter(function($el) use($category){
            return $el->category_id == $category->id;
        });

        $similarAnnouncements = $announcementsForCategory->except($announcement->id);

        return view('announcement.show', compact('announcement', 'similarAnnouncements'));
    }

    

}
