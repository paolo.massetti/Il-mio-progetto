<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RevisorRequest;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        
    }
    
    public function create()
    {
        return view('mail.create');
    }

    public function contact(RevisorRequest $request)
    {
        $name = $request ->input('name');
        $mail = $request ->input('mail');
        $message = $request ->input('message');

        $bag = compact('name', 'mail','message');
     
        $email = new ContactMail($bag);

        Mail::to('paolo@paolo.it')->send($email);

        return redirect()->back()->with('message','Mail inviata correttamente');
    }


}
