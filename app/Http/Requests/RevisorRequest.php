<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RevisorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=> "required|min:3|max:20",
            "mail"=> "required|email:rfc",
            "message"=> "required|min:10|max:200"
        ];
    }

    public function messages()
    {
        return[
            "name.required"=> "Il nome è obbligatorio",
            "name.min"=> "Il nome deve essere lungo almeno 3 caratteri",
            "name.max"=> "Il nome deve essere lungo massimo 10 caratteri",
            "mail.required"=> "La mail è obbligatoria",
            "mail.rfc"=> "La mail deve essere valida es:nome@cognome.it",
            "message.required"=> "Il messaggio è obbligatorio",
            "message.min"=> "Il messaggio deve essere lungo almeno 10 caratteri",
            "message.max"=>"Il messaggio non deve essere più lungo di 200 caratteri"
            
        ];
    }

}
