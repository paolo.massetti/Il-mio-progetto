<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|max:120", 
            "body" =>"required|max:500",
            "price" =>"required|min:1|max:5000"
        ];
    }

    public function messages()
    {
        return [
            
            "title.required" => "il titolo è obbligatorio",
            "title.max" => "il titolo può contenere massimo 120 caratteri",
            "body.required" => "Il contenuto è obbligatorio",
            "body.max" => " il contenuto non può superare i 500 caratteri",
            "price.required" => "l'annuncio deve contenere il prezzo",
            "price.min" => "il prezzo non può essere minore di 1€",
            "price.max" => " il prezzo non può essere superiore a 5000 €"
            

           
        ];
    }
}
