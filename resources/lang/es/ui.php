<?php

return [
    //home
    'welcome' => 'Bienvenido in presto.it',
    'subtitle' => 'Comprar y vender usado',
    'placeholder' => 'Buscar...',
    'search' => 'Buscar!',

    //navbar
    'navCategories' => 'categorias',
    'insertAnnouncement' => 'Empieza a vender',

    //counters
    'announcements' => 'Anuncios',
    'users' => 'Usuarios',
    'counterCategories' => 'Categorias',
    'reviews' => 'Criticas',

    //cards
    'readMore' => 'Lee mas',

    //footer
    'socialLinks' => 'Mantente en contacto con nosotros. Siguenos tambien en nuestras redes sociales',
    'aboutUs' => 'Quienes somos',
    'workWithUs' => 'Trabaja con nosotros',
    'privacy' => 'Privacy',
    'legalNotes' => 'Notas legales y condiciones',
    'contacts' => 'Contactos',
    'newsletterMsg' => 'Suscribete a la newsletter y recibe actualizaciones sobre nuestras novedades',
    'newsletterPlaceholder' => 'Introduzca su direccion de correo electronico',


    //CREATE
    'createAnnouncement' => 'Nuevo anuncio',
    'createTitle' => 'Titulo',
    'createBody' => 'Descripcion',
    'createPrice' => 'Precio en €',
    'createImages' => 'Imagenes',
    'createPlaceholder' => 'Arrastre files aqui para subirlos',
    'createSubmit' => 'Enviar',


    //REVISORHOME
    'revisorAds' => 'Anuncio',
    'revisorUser' => 'Usuario',
    'revisorTitle' => 'Titulo',
    'revisorBody' => 'Descripcion',
    'revisorImages' => 'Imagenes',
    'revisorReject' => 'Rechazar',
    'revisorAccept' => 'Aceptar',
    'noAds' => 'No hay anuncios para revisar',
    'labels' => 'Etiquetas encontradas',


     //SHOW
     'showCategory' => 'Categorìa:',
     'showCreatedAt' => 'Publicado en:',
     'showUser' => 'por:',
];