<?php

return [

    'welcome' => 'Welcome to presto.it',
    'subtitle' => 'Buy and sell used',
    'placeholder' => 'Search...',
    'search' => 'Search!',

    //navbar
    'navCategories' => 'Categories',
    'insertAnnouncement' => 'Start selling',
    'Login' => 'Login',
    'Register' => 'Register',

    //counters
    'announcements' => 'Ads',
    'users' => 'Users',
    'counterCategories' => 'Categories',
    'reviews' => 'Reviews',

    //cards
    'readMore' => 'Read more',

    //footer
    'socialLinks' => 'Keep in touch with us. Follow us on our social channels',
    'aboutUs' => 'About us',
    'workWithUs' => 'Work with us',
    'privacy' => 'Privacy',
    'legalNotes' => 'Legal notes and conditions',
    'contacts' => 'Contacts',
    'newsletterMsg' => 'Subscribe to the newsletter and receive updates on our news',
    'newsletterPlaceholder' => 'Please enter your email address',


    //CREATE
    'createAnnouncement' => 'New ad',
    'createTitle' => 'Title',
    'createBody' => 'Description',
    'createPrice' => 'Price in €',
    'createImages' => 'Images',
    'createPlaceholder' => 'Drop files here to upload',
    'createSubmit' => 'Submit',


    //REVISORHOME
    'revisorAds' => 'Ad',
    'revisorUser' => 'User',
    'revisorTitle' => 'Title',
    'revisorBody' => 'Description',
    'revisorImages' => 'Images',
    'revisorReject' => 'Reject',
    'revisorAccept' => 'Accept',
    'noAds' => 'There are no ads to review',
    'labels' => 'Labels',
    


     //SHOW
     'showCategory' => 'Category:',
     'showCreatedAt' => 'Published on:',
     'showUser' => 'by:',
];