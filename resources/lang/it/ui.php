<?php

return [
    'welcome' => 'Benvenuti in presto.it', 
    'subtitle' => 'Compra e vendi usato',
    'placeholder' => 'Cerca...',
    

    //navbar
    'navCategories' => 'Categorie',
    'insertAnnouncement' => 'Inizia a vendere',
    'Login' => 'accedi',
    'Register' => 'registrati', 

    //counters
    'announcements' => 'Annunci',
    'users' => 'Utenti',
    'counterCategories' => 'Categorie',
    'reviews' => 'Recensioni',

    //cards
    'readMore' => 'Leggi di più',

    //footer
    'socialLinks' => 'Rimani in contatto con noi. Seguici anche sui nostri canali social',
    'aboutUs' => 'Chi siamo',
    'workWithUs' => 'Lavora con noi',
    'privacy' => 'Privacy',
    'legalNotes' => 'Note legali e condizioni',
    'contacts' => 'Contatti',
    'newsletterMsg' => 'Iscriviti alla newsletter e ricevi aggiornamenti sulle nostre novità',
    'newsletterPlaceholder' => 'Inserisci il tuo indirizzo email',



    //CREATE
    'createAnnouncement' => 'Nuovo annuncio',
    'createTitle' => 'Titolo',
    'createBody' => 'Descrizione',
    'createPrice' => 'Prezzo in €',
    'createImages' => 'Immagini',
    'createPlaceholder' => 'Trascina i file qui per caricarli',
    'createSubmit' => 'Invia',


    //REVISORHOME
    'revisorAds' => 'Annuncio',
    'revisorUser' => 'Utente',
    'revisorTitle' => 'Titolo',
    'revisorBody' => 'Descrizione',
    'revisorImages' => 'Immagini',
    'revisorReject' => 'Rifiuta',
    'revisorAccept' => 'Accetta',
    'noAds' => 'Non ci sono annunci da revisionare',
    'labels' => 'Etichette riscontrate',


    //SHOW PRODOTTO
    'showCategory' => 'Categoria:',
    'showCreatedAt' => 'Pubblicato il:',
    'showUser' => 'da:',
];