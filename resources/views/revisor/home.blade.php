@extends('layouts.app')

@section('content')
        @if(session('message'))
        <div class="alert alert-success text-center">
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        @endif

            
    @if($announcement)

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">{{ __('ui.revisorAds') }} # {{ $announcement->id }}</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>{{ __('ui.revisorUser') }}</h3>
                                </div>
                                <div class="col-md-10">
                                    # {{ $announcement->user->id }}, 
                                    {{ $announcement->user->name}},
                                    {{ $announcement->user->email }}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>{{ __('ui.revisorTitle') }}</h3>
                                </div>
                                <div class="col-md-10">
                                    {{ $announcement->title }}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>{{ __('ui.revisorBody') }}</h3>
                                </div>
                                <div class="col-md-10">
                                    {{ $announcement->body }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>{{ __('ui.revisorImages') }}</h3>
                                </div>
                                <div class="col-md-10">
                                    @foreach ($announcement->announcementImages as $image)
                                        <div class="row mb-2">
                                                <div class="col-md-4">
                                                    {{-- <img class="img-fluid" src="{{ Storage::url($image->file) }}" alt=""> --}}
                                                    <img  src="{{ $image->getUrl(300, 230) }}" alt="">

                                                </div>
                                        
                                                <div class="col-md-4">
                                                    
                                                    <strong class="ml-3">Adult:</strong> {{ $image->adult }} <br>
                                                    <strong class="ml-3">Spoof:</strong> {{ $image->spoof }} <br>
                                                    <strong class="ml-3">Medical:</strong> {{ $image->medical }} <br>
                                                    <strong class="ml-3">Violence:</strong> {{ $image->violence }} <br>
                                                    <strong class="ml-3">Racy:</strong> {{ $image->racy }} <br>
                                                    
                                                    {{-- {{ $image->id }} <br>
                                                    {{ $image->file }} <br>
                                                    {{ Storage::url($image->file) }} <br> --}}
                                                    <h5 class="mt-5 mb-0 mx-3">{{ __('ui.labels') }}</h5> <br>
                                                    <ul>
                                                        @if ($image->labels)
                                                            @foreach ($image->labels as $label)
                                                                <li>{{ $label }}</li>
                                                            @endforeach
                                                            
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    @if ($image->adult == 'VERY_UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->adult == 'UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->adult == 'POSSIBLE')
                                                        <span class="d-block"><i class="fas fa-exclamation-triangle text-warning"></i></span>
                                                    @elseif ($image->adult == 'LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @elseif ($image->adult == 'VERY_LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @else

                                                    @endif

                                                    @if ($image->spoof == 'VERY_UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->spoof == 'UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->spoof == 'POSSIBLE')
                                                        <span class="d-block"><i class="fas fa-exclamation-triangle text-warning"></i></span>
                                                    @elseif ($image->spoof == 'LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @elseif ($image->spoof == 'VERY_LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @else

                                                    @endif

                                                    @if ($image->medical == 'VERY_UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->medical == 'UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->medical == 'POSSIBLE')
                                                        <span class="d-block"><i class="fas fa-exclamation-triangle text-warning"></i></span>
                                                    @elseif ($image->medical == 'LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @elseif ($image->medical == 'VERY_LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @else

                                                    @endif

                                                    @if ($image->violence == 'VERY_UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->violence == 'UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->violence == 'POSSIBLE')
                                                        <span class="d-block"><i class="fas fa-exclamation-triangle text-warning"></i></span>
                                                    @elseif ($image->violence == 'LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @elseif ($image->violence == 'VERY_LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @else

                                                    @endif

                                                    @if ($image->racy == 'VERY_UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->racy == 'UNLIKELY')
                                                        <span class="d-block"><i class="fas fa-check-circle text-success"></i></span>
                                                    @elseif ($image->racy == 'POSSIBLE')
                                                        <span class="d-block"><i class="fas fa-exclamation-triangle text-warning"></i></span>
                                                    @elseif ($image->racy == 'LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @elseif ($image->racy == 'VERY_LIKELY')
                                                        <span class="d-block"><i class="fas fa-exclamation-circle text-danger"></i></span>
                                                    @else

                                                    @endif


                                                    
                                                </div>
                                                
                                        </div>
                                    @endforeach        
                                    
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center mt-5">
                <div class="col-md-6">
                    <form action="{{ route('revisor.reject', $announcement->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-danger mb-5">{{ __('ui.revisorReject') }}</button>
                    </form>
                </div>

                <div class="col-md-6 text-right">
                    <form action="{{ route('revisor.accept', $announcement->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-success mb-5">{{ __('ui.revisorAccept') }}</button>
                    </form>    
                </div>

                
            </div>
        </div>
    @else
        <div class="container my-5 py-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <h3>{{ __('ui.noAds') }}</h3>
                </div>
            </div>
        </div>
    @endif


@endsection
                                
                                
                    
