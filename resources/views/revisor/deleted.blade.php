@extends('layouts.app')

@section('content')
    
    @if(count($announcements) > 0)
        <div class="container my-5 py-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table table-bordered table-responsive-lg">
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>body</th>
                            <th>price</th>
                            <th>Date Created</th>
                            <th>Action</th>
                            
                        </tr>
                        @foreach ($announcements as $announcement)
                            <tr>
                                <td>{{ $announcement->id }}</td>
                                <td>{{ $announcement->title }}</td>
                                <td>{{ $announcement->body }}</td>
                                <td>{{ $announcement->price }}</td>
                                
                                <td>{{ $announcement->created_at->format('d/m/Y') }}</td>
                                
                                <td class="d-flex justify-content-between">
                                    
                                        <form action="{{ route('revisor.accept', $announcement->id)}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-success"><i class="fas fa-undo"></i></button>
                                        </form>  
                                        <form action="{{ route('announcement.destroy', $announcement) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                       
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="container my-5 py-5">
            <div class="row justify-conten-center">
                <div class="col-12">
                    <h3>Il cestino è vuoto</h3>
                </div>
            </div>
        </div>
    @endif
    


@endsection