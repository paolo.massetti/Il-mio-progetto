@extends('layouts.app')

@section('content')
@if(session('status'))
    <div class="alert alert-success mb-0">
        {{ session('status') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
@endif

@if(session('access.denied.revisor.only'))
    <div class="alert alert-danger">
      Accesso consentito ai soli revisori
    </div>
@endif

<div class="container-fluid masthead pt-0 d-none d-sm-block">
    <div class="container h-100 ml-3">
      <div class="row h-100 align-items-center">
        <div class="col-md-8">
            <div class="card-custom bg-white d-none d-sm-block rounded-0 p-1 border border-dark" style="width: 24rem;">
                <div class="card-body text-center">
                    <h2 class="card-title text-dark">E' arrivato il momento di guadagnare!</h2>
                    <button type="button" class="btn button-custom bg-accent btn-lg mt-3 font-weight-bold my-2"><a href="{{ route('announcement.create') }}">{{ __('ui.insertAnnouncement') }}</a></button>
                </div>
            </div>
        </div>
      </div>

      
      {{-- <div class="row align-items-center h-100">
        <div class="col-12 col-md-5 mb-5">
          <h1>{{ __('ui.welcome') }}</h1>
          <h2>{{ __('ui.subtitle') }}</h2>
        
              <form action="{{route('search')}}" method="GET">
                <div class="form-group">
                    <input type="text" name="q" class="form-control" placeholder="{{ __('ui.placeholder') }}">
                </div>
                  <button class="btn btn-warning" type="submit">{{ __('ui.search') }}!</button>
            </form>
        </div>
      </div> --}}
    </div>
</div>  

<div class="container-fluid masthead-mobile pt-0 d-block d-sm-none"></div>
<div class="container d-block d-sm-none my-5">
  <div class="row">
      <div class="col-12">
          <h2 class="text-center">E' arrivato il momento di guadagnare!</h2>
          <button type="button" class="btn bg-accent btn-lg btn-block mt-3 text-white">{{ __('ui.insertAnnouncement') }}</button>
      </div>
  </div>
</div>

<div class="container-fluid my-5">
  <div class="row align-items-center">
    <div class="col-12 col-md-3">
      <hr class="bg-danger">
    </div>
    <div class="col-md-3 text-center">
      <h4 class="text-danger mt-3 text-wrap text-uppercase font-weight-bold">Cerca per categoria</h4>
    </div>
    <div class="col-md-6">
      <hr class="bg-danger">
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    
      <div class="col-6 col-md-3">
          <div class="card-category-1 rounded mb-3 shadow">
              <div class="card-body rounded d-flex justify-content-center bg-white text-danger">
                  
                  <div class="text-center"><i class="fas fa-music"></i>                    
                      <h5 class="card-title">Musica</h5>
                     
                        <a href="{{ route('announcements.category', 
                        ['id' => 7,
                        'name' => 'musica']
                        ) }}" class="button-card-1 btn btn-transparent px-4 text-nowrap">Cerca</a>    
                      
                  </div>
              </div>
          </div>
      </div>
    
      <div class="col-6 col-md-3">
          <div class="card-category-2 rounded  mb-3 shadow">
              <div class="card-body rounded d-flex justify-content-center text-warning bg-white">
                  
                  <div class="text-center"><i class="fas fa-volleyball-ball"></i>
                      <h5 class="card-title">Sport</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 8,
                      'name' => 'sport']
                      ) }}" class="button-card-2 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-6 col-md-3">
          <div class="card-category-3 rounded mb-3 shadow">
              <div class="card-body card-body-3 rounded d-flex justify-content-center bg-white" >
                  
                      <div class="text-center"><i class="fas fa-shopping-bag mr-2"></i>
                          <h5 class="card-title">Abbigliamento</h5>
                          <a href="{{ route('announcements.category', 
                          ['id' => 1,
                          'name' => 'abbigliamento']
                          ) }}" class="button-card-3 btn btn-transparent px-4">Cerca</a>
                      </div>
                  </div>
              </div>
          </div>
                          

      <div class="col-6 col-md-3">
          <div class="card-category-4 rounded mb-3 shadow">
              <div class="card-body rounded d-flex justify-content-center text-success bg-white">
                  
                  <div class="text-center"><i class="fas fa-couch"></i>
                      <h5 class="card-title">Arredamento</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 2,
                      'name' => 'arredamento']
                      ) }}" class="button-card-4 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-6 col-md-3">
          <div class="card-category-5 rounded mb-3 shadow">
              <div class="card-body rounded d-flex justify-content-center text-main bg-white">
                  
                  <div class="text-center"><i class="fas fa-motorcycle"></i>
                      <h5 class="card-title">Motori</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 6,
                      'name' => 'motori']
                      ) }}" class="button-card-5 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-6 col-md-3">
          <div class="card-category-6 rounded mb-3 shadow">
              <div class="card-body card-body-6 rounded d-flex justify-content-center bg-white">
                  
                  <div class="text-center"><i class="fas fa-laptop"></i>
                      <h5 class="card-title text-center">Elettronica</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 3,
                      'name' => 'elettronica']
                      ) }}" class="button-card-6 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-6 col-md-3">
          <div class="card-category-7 mb-3 shadow">
              <div class="card-body rounded d-flex justify-content-center text-accent bg-white">
                  
                  <div class="text-center"><i class="fas fa-home"></i>
                      <h5 class="card-title">Immobili</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 5,
                      'name' => 'immobili']
                      ) }}" class="button-card-7 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>           
      <div class="col-6 col-md-3 mb-5">
          <div class="card-category-8 rounded mb-3 shadow">
              <div class="card-body card-body-8 rounded d-flex justify-content-center bg-white">
                  
                  <div class="text-center"><i class="fas fa-briefcase"></i>
                      <h5 class="card-title">Lavoro</h5>
                      <a href="{{ route('announcements.category', 
                      ['id' => 4,
                      'name' => 'lavoro']
                      ) }}" class="button-card-8 btn btn-transparent px-4">Cerca</a>
                  </div>
              </div>
          </div>
      </div>             
  </div>
</div>

<div class="container-fluid my-5 bg-light">
    <div class="row align-items-center">
      <div class="col-12 col-md-3">
        <hr class="bg-danger">
      </div>
      <div class="col-md-2 text-center">
        <h4 class="text-danger mt-3 text-uppercase font-weight-bold">le novità</h4>
      </div>
      <div class="col-md-7">
        <hr class="bg-danger">
      </div>
    </div>
</div>



    

{{-- card --}}
<div class="container my-5">
    <div class="row row-cols-1 row-cols-md-3 mt-5">
        @foreach ($announcements as $announcement)
            <div class="col-12 col-md-6 col-xl-4 mb-4">
                <div class="card h-100 shadow">
                    <div class="card-header bg-white">
                      <small><i class="fas fa-user pr-2"></i>{{ $announcement->user->name }}</small>
                    </div>
                    <div class="inner">
                        <a href="{{ route('announcement.show', $announcement) }}">    
                            {{-- <img class="img-fluid" src="{{ Storage::url($image->file) }}" class="card-img-top" alt="..."> --}}
                            <img src="{{ $announcement->getCover( 300, 230) }}" class="card-img-top" alt="...">
                        </a>    
                    </div>
                        
                    <div class="card-body p-3">
                        <div class="d-flex justify-content-between">
                            <h4 class="font-weight-bolder my-2">€ {{ $announcement->price}}</h4>
                            <a href="{{ route('announcements.category', [$announcement->category->name, $announcement->category->id]) }}">
                                <button class="btn bg-danger py-0 my-2 label-custom font-weight-bold text-uppercase text-white">{{$announcement->category->name}}</button>
                            </a>
                        </div>
                        <a href="{{ route('announcement.show', $announcement) }}" class="text-decoration-none">
                            <h5 class="card-title text-dark font-weight-bold mt-2">{{ $announcement->title }}</h5>
                        </a>
                            <p class="card-text mt-4 text-truncate">{{ $announcement->body }}</p>
                    </div>
                    <div class="card-footer bg-white d-flex justify-content-between py-2">
                        <a href="{{ route('announcement.show', $announcement) }}">
                            <p class="font-weight-bold">{{ __('ui.readMore') }}</p>
                        </a>
                        <i class="text-muted">{{ $announcement->created_at->format('d/m/Y') }}</i>
                    </div>    
                </div>
            </div>
        @endforeach
    </div>
</div>

<button id="BtnTop" class="animate slideIn"><i class="fas fa-chevron-up p-2"></i></button>
@endsection

    
                            
                            


    

    
              
    





                                
                                
                            
                                    
                                    
                                
        
            
            
                
            
