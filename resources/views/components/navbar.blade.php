<nav id="navbar" class="navbar navbar-expand-lg  fixed-top shadow-sm">
    <div class="container">
        <a class="navbar-brand font-weight-bold text-accent" href="{{ url('/') }}">Presto.it
            {{-- {{ config('app.name', 'Laravel') }} --}}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <i class="fas fa-bars fa-lg"></i>
        </button>
            

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a id="categoriesDropdown" class="nav-link text-dark font-weight-bold dropdown-toggle" href="" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>{{ __('ui.navCategories') }}<span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right animate slideIn rounded-0" aria-labelledby="categoriesDropdown">
                        @foreach($categories as $category)
                            <a class="dropdown-item" 
                                href="{{ route('announcements.category', [
                                    $category->name,
                                    $category->id
                                    ]) }}">{{ $category->name }}       
                            </a>
                        @endforeach
                    </div>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            {{-- searchbar --}}
            <ul class="navbar-nav ml-auto">
                <li class="nav-item my-0 py-0">
                    <form action="{{route('search')}}" method="GET" class="form-inline my-2 my-lg-0">
                        <div class="form-group d-flex">
                            <input type="text" name="q" id="searchBar"  class="form-control rounded-0 border-secondary" placeholder="{{ __('ui.placeholder') }}">
                            
                                <button class="btn rounded-0 border-left-0 border-secondary" type="submit"><i class="px-3 fas fa-search"></i></button>
                            
                        </div>
                    </form>
                </li>

                
                
        
                {{-- flags --}}
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto" id="flagLink">
                        <li class="nav-item dropdownd-flex">
                            <form action="{{ route('locale', 'it') }}" method="POST">
                                @csrf
                                <button type="submit" class="nav-link" style="background-color:transparent; border:none;">
                                    <span class="flag-icon flag-icon-it"></span>
                                </button>
                            </form>
                        </li>
                        <li class="nav-item-flex">
                            <form action="{{ route('locale', 'en') }}" method="POST">
                                @csrf
                                <button type="submit" class="nav-link" style="background-color:transparent; border:none;">
                                    <span class="flag-icon flag-icon-gb"></span>
                                </button>
                            </form>
                        </li>
                        <li class="nav-item-flex">
                            <form action="{{ route('locale', 'es') }}" method="POST">
                                @csrf
                                <button type="submit" class="nav-link" style="background-color:transparent; border:none;">
                                    <span class="flag-icon flag-icon-es"></span>
                                </button>
                            </form>
                        </li>
                    </ul>        
                </div>   

                        
                              


                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link text-dark font-weight-bold" href="{{ route('login') }}">{{ __('ui.Login') }}</a>
                        </li>
                    @endif
                    
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-dark font-weight-bolder" href="{{ route('register') }}">{{ __('ui.Register') }}</a>
                        </li>
                    @endif
                @else
                        {{-- revisore --}}
                @if(Auth::user()->is_revisor)
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-dark font-weight-bold" href="" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Revisor Area
                            <span class="badge badge-pill badge-warning">{{ \App\Models\Announcement::ToBeRevisionedCount() }}</span>
                        </a>
                        <div class="dropdown-menu animate slideIn" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('revisor.home') }}">Home</a>
                            <a class="dropdown-item" href="{{ route('revisor.deleted') }}">Cestino</a>
                        </div>
                    </li>
                @endif
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark font-weight-bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>