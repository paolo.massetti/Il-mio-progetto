<footer class="footer">
    <div class="container-fluid mx-auto p-5 bg-fourth">
      <div class="row py-4">
        <div class="col-12 col-md-4 pr-0 mb-4 mb-lg-0">
          <h5 class="logofooter text-accent font-weight-bold mb-4">Presto.it</h5>
          <p class="font-italic text-fifth">{{ __('ui.socialLinks') }}</p>
          <ul class="list-inline mt-4">
            <li class="list-inline-item"><a href="#" target="_blank" title="twitter"><i class="fab fa-twitter fa-lg text-accent"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="facebook"><i class="fab fa-facebook-f fa-lg ml-4 text-accent"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="instagram"><i class="fab fa-instagram fa-lg ml-4 text-accent"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="linkedin"><i class="fab fa-linkedin fa-lg ml-4 text-accent"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="whatsapp"><i class="fab fa-whatsapp fa-lg ml-4 text-accent"></i></a></li>
          </ul>
        </div>
        <div class="col-12 col-md-4 pl-5 mb-4 mb-lg-0">
          <h6 class="text-uppercase text-dark font-weight-bold mb-4">MORE</h6>
          <ul class="list-unstyled mb-0">
            <li class="mb-2 li-footer"><a href="#" class="text-fifth">{{ __('ui.aboutUs') }}</a></li>
            <li class="mb-2"><a href="{{ route('mail.create') }}" class="text-fifth">{{ __('ui.workWithUs') }}</a></li>
            <li class="mb-2"><a href="#" class="text-fifth">{{ __('ui.privacy') }}</a></li>
            <li class="mb-2"><a href="#" class="text-fifth">{{ __('ui.legalNotes') }}</a></li>
            <li class="mb-2"><a href="#" class="text-fifth">{{ __('ui.contacts') }}</a></li>
          </ul>
        </div>
        <div class="col-12 col-md-4 mb-lg-0">
          <h6 class="text-uppercase text-dark font-weight-bold mb-4">NEWSLETTER</h6>
          <p class="text-fifth font-italic mb-4">{{ __('ui.newsletterMsg') }}</p>
          <div class="p-2 rounded-pill bg-white">
            <div class="input-group">
              <input type="email" placeholder="{{ __('ui.newsletterPlaceholder') }}" aria-describedby="button-addon1" class="form-control border-0 bg-transparent shadow-0">
              <div class="input-group-append">
                <button id="button-addon1" type="submit" class="btn btn-link"><i class="fa fa-envelope fa-lg text-dark"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Copyrights -->
    <div class="bg-dark text-white py-4">
      <div class="container text-center">
        <p class="text-muted mb-0 py-2">© 2021 Presto.it All rights reserved.</p>
      </div>
    </div>
  </footer>