@extends('layouts.app')

@section('content')
    @if(session('message'))
    <div class="alert alert-success text-center">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 mt-3">
        
                <h2 class="text-center my-3">Diventa revisore del sito</h2>
        
                <form  method="POST" action="{{route('contact.send')}}">
                    @csrf
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
            
                    </div>
                    <div class="form-group">
                        <label for="">Mail</label>
                        <input type="email" class="form-control"  name="mail" value="{{old('mail')}}">
                        @error('mail')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        
                    </div>
                    <div class="form-group">
                        <label  for="">Messaggio</label>
                        <textarea class="form-control" name="message" id="" cols="30" rows="10">{{old('message')}}</textarea>
                        @error('message')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary mt-2 mb-5">Candidati</button>
                </form>
        
            </div>    
        </div>
    </div>
    
@endsection