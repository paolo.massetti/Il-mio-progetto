@extends('layouts.app')

@section('content')
    <div class="container my-5 py-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
              <h2 class="card-header">{{ __('ui.createAnnouncement') }}</h2>
              
              <form method="POST" action="{{route('announcement.store')}}">
                @csrf

                <input type="hidden" name="uniqueSecret" value="{{ $uniqueSecret }}" />
  
                <div class="form-group">
                  <select name="category_id"  class="form-control"  value="{{old('category_id')}}" >
                    @foreach ($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
  
                <div class="form-group">
                  <label for="title">{{ __('ui.createTitle') }}</label>
                  <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                  @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                
                <div class="form-group">
                  <label for="body">{{ __('ui.createAnnouncement') }}</label>
                  <textarea name="body" class="form-control" cols="30" rows="10">{{ old('body') }}</textarea>
                  @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                
                <div class="form-group">
                  <label for="price">{{ __('ui.createPrice') }}</label>
                  <input type="number" name="price" step="0.01" min="1" max="5000" class="form-control" value="{{ old('price') }}">
                  
                  @error('price')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                  {{-- dropzone --}}
                <div class="form-group">
                  <label for="images" class="col-form-label">{{ __('ui.createImages') }}</label>
                  <div class="dropzone" id="drophere"></div>
                  @error('images')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <button type="submit" class="btn btn-primary">{{ __('ui.createSubmit') }}</button>
              </form>
              
            </div>
        </div>
    </div>
@endsection


                      

                      

                      
                   
                          
          
                    

