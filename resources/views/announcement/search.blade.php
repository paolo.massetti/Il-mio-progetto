@extends('layouts.app')

@section('content')

    <div class="container-fluid my-5">
        <div class="row align-items-center">
        <div class="col-12 col-md-2">
            <hr class="bg-danger">
        </div>
        <div class="col-md-3 text-center">
            <h3 class="text-danger mt-3 text-wrap text-uppercase font-weight-bold">Risultati per:</h3>
            <p class="lead font-weight-bold">{{ $q }}</p>
        </div>
        <div class="col-md-7">
            <hr class="bg-danger">
        </div>
        </div>
    </div>


<div class="container  m-4 p-4 bg-light shadow mx-auto">
    <div class="row justify-content-center">
        @foreach ($announcements as $announcement)
            <div class="col-md-8 card-category">
                <a href="{{ route('announcement.show', $announcement) }}" class="text-dark">
                    <div class="card shadow p-3 mb-5 rounded-0 bg-fourth">
                        <div class="row no-gutters">
                            <div class="col-lg-4">
                                <img src="{{ $announcement->getCover(300, 230) }}" class="card-img rounded-0" alt="...">        
                            </div>
                                <div class="col-lg-8">
                                    <div class="card-body">
                                        <h4 class="card-title font-weight-bold mb-2">{{$announcement->title}}</h4>
                                        <i class="text-muted font-weight-bold">{{ $announcement->created_at->format('d/m/Y') }}</i>
                                        <p class="text-truncate my-3">{{ $announcement->body }}</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <h4 class="font-weight-bolder text-danger">€ {{ $announcement->price}}</h4>
                                            <small><i class="fas fa-user pr-2"></i>{{ $announcement->user->name }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>           
                </a>
            </div>   
        @endforeach
    </div>
</div>



    
@endsection