@extends('layouts.app')

@section('content')
    {{-- carousel --}}
    <div class="container m-4 p-4 bg-white shadow mx-auto">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 mt-3">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach ($announcement->announcementImages as $image)
                            <div class="carousel-item {{ $loop->first ? 'active' : ''}}">
                                <img src="{{ $image->getUrl(480, 380) }}" class="d-block w-100" alt="...">
                            </div>
                    
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>  
            </div>

            <div class="col-12 col-lg-4 mt-3">
                <div class="d-flex justify-content-between align-items-start">
                    <i class="text-muted font-weight-bold">{{ $announcement->created_at->format('d/m/Y') }}</i>
                    <p><i class="fas fa-user pr-2"></i>{{ $announcement->user->name }}</p>
                </div>
                    <h3 class="font-weight-bold my-3 text-wrap">{{ $announcement->title }}</h3>
                    <div class="d-flex justify-content-between align-items-center pb-5 border-bottom">
                        <h3 class="font-weight-bold text-danger mt-5">{{ $announcement->price }} €</h3>
                        <a href="{{ route('announcements.category', [$announcement->category->name, $announcement->category->id]) }}">
                            <button class="btn bg-danger py-0 my-2 mt-5 label-custom font-weight-bold text-uppercase text-white">{{$announcement->category->name}}</button>
                        </a>
                    </div>
                    <div class="my-3">
                        <h5 class="lead font-weight-bolder">Descrizione:</h5>
                        <p class="lead">{{$announcement->body }}</p>
                    </div>
            </div>
        </div>
    </div>
                
     
                
    
            
            
            
            

    {{-- annunci simili --}}

    <div class="container-fluid my-5">
        <div class="row align-items-center">
          <div class="col-12 col-md-2">
            <hr class="bg-danger">
          </div>
          <div class="col-md-3 text-center">
            <h4 class="text-danger mt-3 text-wrap text-uppercase font-weight-bold">correlati</h4>
          </div>
          <div class="col-md-7">
            <hr class="bg-danger">
          </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="similar-products">
                    @foreach($similarAnnouncements as $similarAnnouncement)
                        <div class="card shadow-sm p-3 mb-5 rounded-0 bg-fourth">
                            <div class="row no-gutters">
                            <div class="col-lg-4">
                                <img src="{{ $similarAnnouncement->getCover(300, 230) }}" class="card-img rounded-0" alt="...">
                            </div>
                            <div class="col-lg-8">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold mb-2">{{ $similarAnnouncement->title }}</h5>
                                    <i class="text-muted font-weight-bold">{{ $similarAnnouncement->created_at->format('d/m/Y') }}</i>
                                    <p class="text-truncate my-3">{{ $similarAnnouncement->body }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <h4 class="font-weight-bolder text-danger">€ {{ $similarAnnouncement->price}}</h4>
                                        <small><i class="fas fa-user pr-2"></i>{{ $similarAnnouncement->user->name }}</small>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

   
    
    
@endsection
