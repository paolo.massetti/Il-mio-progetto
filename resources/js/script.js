let navbar = document.querySelector('#navbar');
let categoriesDropdown = document.querySelector('#categoriesDropdown');
let flagLink = document.querySelector('#flagLink');



if(window.innerWidth > 1000) {
    document.addEventListener('scroll',() => {
        if(window.pageYOffset > 50) {
            navbar.classList.remove('bg-transparent')
            navbar.classList.add('bg-fourth')
            categoriesDropdown.classList.add('d-none')
            flagLink.classList.add('d-none')
            

        } else {
            navbar.classList.remove('bg-fourth')
            navbar.classList.add('bg-transparent')
            categoriesDropdown.classList.remove('d-none')
            flagLink.classList.remove('d-none')
            

        }
    })
} else {
    navbar.classList.add('bg-fourth')
}

$(document).ready(function(){
    $('.similar-products').slick({
        
        dots: true,
        fade: true,
        cssEase: 'linear',
        arrows: true
       
    });
});

let scrollToTopBtn = document.getElementById("BtnTop");

let rootElement = document.documentElement

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 1200 || document.documentElement.scrollTop > 1200) {
    scrollToTopBtn.style.display = "block";
  } else {
    scrollToTopBtn.style.display = "none";
  }
}

function scrollToTop() {
  
  rootElement.scrollTo({
    top: 0,
    behavior: "smooth"
  })
}
scrollToTopBtn.addEventListener("click", scrollToTop)
        
        
